import java.util.UUID

import org.scalatest.{BeforeAndAfterAll, FunSpec}
import com.datastax.spark.connector._
import org.apache.spark.{SparkConf, SparkContext}
import Setup.connect
import Main.findQuery
import Main.toSparkDataType
import Model.{Detail, Query}
import Main._
import com.datastax.driver.core.utils.UUIDs
import com.holdenkarau.spark.testing.{DataFrameSuiteBase, DataframeGenerator}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SQLContext, Column}

import scala.util.Random

/**
  * Created by tokevonryberg on 22/04/16.
  */
class MainSpec extends FunSpec with BeforeAndAfterAll {

  val sc: SparkContext = connect
  val sqlContext: SQLContext = new SQLContext(sc)

  import sqlContext.implicits._

  val existingSiloId = UUID.fromString("d9e7bfb0-1c1a-11e6-a606-555a1fa8c3db")
  val existingQueryId = UUID.fromString("dc10e141-1c1a-11e6-a606-555a1fa8c3db")
  val whereClause = Map(("base_data", "height") -> "<4", ("patient", "birthday") -> "LIKE %-10-1970")
  val entityDetailTuples = Set[(String, String)](("base_data", "height"),
    ("patient", "birthday"),
    ("base_data", "weight"))

  val detailsRdd = sc.parallelize(List[Detail](Detail("base_data", "height", "double"),
    Detail("patient", "birthday", "date"),
    Detail("base_data", "weight", "double")))
  val containerSchema = StructType(List(StructField("entity_id", StringType),
    StructField("container_id", StringType),
    StructField("x1", MapType.apply(StringType, StringType))))
  val containers = sqlContext.createDataFrame(sc.parallelize(List[Row](Row("base_data", "8086f6a0-1e7e-11e6-8927-8506c54465ef", Map("height" -> "100", "weight" -> "40")),
    Row("blood_samples", "dbf43180-1c1a-11e6-a606-555a1fa8c3db", Map("height" -> "200", "weight" -> "100")),
    Row("base_data", "4163c930-1e7e-11e6-9029-8506c54465ef", Map("height" -> "200", "weight" -> "100")))), containerSchema)


  override def afterAll = {
    sc.stop()
  }

  describe("main") {
    it("must throw an IllegalArgumentException if not called with 2 arguments") {
      intercept[IllegalArgumentException] {
        main(Array("1", "2", "3", "4", "5"))
      }
      intercept[IllegalArgumentException] {
        main(Array("1", "2", "3"))
      }
      intercept[IllegalArgumentException] {
        main(Array("1"))
      }
    }

    it("must throw an IllegalArgumentException if any argument is of wrong type(UUID (Time),UUID (Time)") {
      intercept[IllegalArgumentException] {
        main(Array(existingSiloId.toString, "notRemotelyLikaAnUUID"))
      }
      intercept[IllegalArgumentException] {
        main(Array("notRemotelyLikaAnUUID", existingQueryId.toString))
      }
    }

  }

  describe("findQuery") {
    it("should return a query with the given siloId and queryId") {
      val sut = findQuery(sc, existingSiloId, existingQueryId)
      assert(sut.isInstanceOf[Query])
      assert(sut.siloId == existingSiloId)
      assert(sut.queryId == existingQueryId)
    }

    it("must throw an NoSuchElementException if query does not exist") {
      intercept[NoSuchElementException] {
        findQuery(sc, existingSiloId, UUIDs.timeBased())
      }
      intercept[NoSuchElementException] {
        findQuery(sc, UUIDs.timeBased(), existingQueryId)
      }
      intercept[NoSuchElementException] {
        findQuery(sc, UUIDs.timeBased(), UUIDs.timeBased())
      }
    }
  }

  describe("getEntityDetailTuplesFromQuery") {

    it("should return a map with entityIds, detailId pairs from Query") {
      val sut = getEntityDetailTuplesFromQuery("base_data", "weight", whereClause)
      assert(sut.isInstanceOf[Set[(String, String)]])
      val expected = Set[(String, String)](("base_data", "height"), ("patient", "birthday"), ("base_data", "weight"))
      assert(sut.equals(expected))
    }

    it("should return distinct entityIds, detailIds pairs") {
      val sut = getEntityDetailTuplesFromQuery("base_data", "height", whereClause)
      assert(sut.isInstanceOf[Set[(String, String)]])
      val expected = Set[(String, String)](("base_data", "height"), ("patient", "birthday"))
      assert(sut.equals(expected))
    }
  }

  describe("findDetailsRdd") {
    it("should find details from entityDetailTuples") {
      val sut = findDetailsRdd(sc, existingSiloId, entityDetailTuples)
      assert(sut.isInstanceOf[RDD[Detail]])
      assert(sut.count() == 3)
    }
  }

  describe("createWhereClauseString") {
    it("should return a string with the where clause") {
      val sut = createWhereClauseString(detailsRdd, whereClause)
      val expected = "(entity_id = 'base_data' and cast(x1.height as double) <4) and (entity_id = 'patient' and " +
        "cast(x1.birthday as date) LIKE %-10-1970)"
      assert(sut.isInstanceOf[String])
      assert(sut.equals(expected))
    }
  }

  describe("findContainersDf") {
    it("should return a DataFrame with all containers from silo") {
      val sut = findContainersDf(sqlContext, existingSiloId.toString)
      assert(sut.isInstanceOf[DataFrame])
      assert(sut.count() == 40)
    }
  }

  describe("filterContainers") {
    it("should return an empty Array of UUID if whereClause is empty") {
      val sut = filterContainers(containers, "")
      assert(sut.isInstanceOf[List[String]])
      assert(sut.size == 0)
    }
    it("should return a Array of UUID whereClause is not empty") {
      val parsedWhereClause = "(entity_id = 'base_data' and cast(x1.height as double) < 160)"
      val sut = filterContainers(containers, parsedWhereClause)
      assert(sut.isInstanceOf[List[String]])
      assert(sut.size == 1)
      assert(sut(0).equals("8086f6a0-1e7e-11e6-8927-8506c54465ef"))
    }
  }

  describe("findContainerAssociations") {
    it("should return a DataFrame with the column to_container_id with all associations from silo and containers") {
      val containerIds = List[String]("dbe898c0-1c1a-11e6-a606-555a1fa8c3db")
      val sut = findContainerAssociations(sqlContext, existingSiloId.toString, "base_data", containerIds)
      assert(sut.isInstanceOf[DataFrame])
      assert(sut.count() == 1)
      assert(sut.col("to_container_id").isInstanceOf[Column])
    }
  }

  describe("getDetailFromContainers") {
    it("should return a dataframe containing a detail for an entity from all containers if containerAssociations are empty") {
      val emptyContainers = sqlContext.createDataFrame(sc.parallelize((List[Row]())), containerSchema)
      val entityId = "base_data"
      val detailId = "height"

      val sut = getDetailFromContainers(containers, entityId, detailId, emptyContainers)
      val detail = sut.collect()
      assert(sut.isInstanceOf[DataFrame])
      assert(sut.schema.size == 1)
      assert(sut.schema(0).name.equals(detailId))
      assert(detail.length == 2)
      assert(detail(0).getString(0).equals("100"))
      assert(detail(1).getString(0).equals("200"))
    }

    it("should return a dataframe containing a detail for an entity from containers in containerAssociations") {
      val containerAssociationSchema = StructType(List(StructField("to_container_id", StringType)))
      val containerAssociations = sqlContext.createDataFrame(
        sc.parallelize(
          List[Row](
            Row("dbf43180-1c1a-11e6-a606-555a1fa8c3db"),
            Row("8086f6a0-1e7e-11e6-8927-8506c54465ef")
          )
        ), containerAssociationSchema)

      val entityId = "base_data"
      val detailId = "height"

      val sut = getDetailFromContainers(containers, entityId, detailId, containerAssociations)
      val detail = sut.collect()
      assert(sut.isInstanceOf[DataFrame])
      assert(sut.schema.size == 1)
      assert(sut.schema(0).name.equals(detailId))
      assert(detail.length == 1)
      assert(detail(0).getString(0).equals("100"))
    }
  }

  describe("getDataSummary") {
    it("should return a data frame with min, avg, max and count for a detail") {
      val detailId = "height"
      val schema = StructType(List(StructField(detailId, DoubleType)))
      val dataSummaryDf = sqlContext.createDataFrame(sc.parallelize(List[Row](Row(1.0), Row(2.0), Row(3.0), Row(2.0))), schema)
      val sut = getDataSummary(dataSummaryDf, detailId)
      val dataSummary = sut.collect()

      assert(sut.isInstanceOf[DataFrame])
      assert(dataSummary.length == 1)
      assert(!dataSummary(0).anyNull)
      assert(sut.schema(0).name.equals("min"))
      assert(dataSummary(0).getDouble(0).equals(1.0))
      assert(sut.schema(1).name.equals("avg"))
      assert(dataSummary(0).getDouble(1).equals(2.0))
      assert(sut.schema(2).name.equals("max"))
      assert(dataSummary(0).getDouble(2).equals(3.0))
      assert(sut.schema(3).name.equals("count"))
      assert(dataSummary(0).getDouble(3).equals(4.0))
    }

    it("should return a data frame with count for a detail with data type string") {
      val detailId = "education"
      val schema = StructType(List(StructField(detailId, StringType)))
      val dataFrequenceDf = sqlContext.createDataFrame(sc.parallelize(List[Row](Row("School"), Row("School"), Row("University"), Row("School"))), schema)
      val sut = getDataSummary(dataFrequenceDf, detailId)
      val dataSummary = sut.collect()

      assert(sut.isInstanceOf[DataFrame])
      assert(dataSummary.length == 1)
      assert(dataSummary(0).isNullAt(0))
      assert(dataSummary(0).isNullAt(1))
      assert(dataSummary(0).isNullAt(2))
      assert(dataSummary(0).getDouble(3).equals(4.0))
    }

    //check that only count is returned when detail is a string
  }

  describe("getDataFrequence") {
    it("should return a data frame with frequence of a detail") {
      val detailId = "height"
      val schema = StructType(List(StructField(detailId, DoubleType)))
      val containerDf = sqlContext.createDataFrame(sc.parallelize(List[Row](Row(1.0), Row(2.0), Row(3.0), Row(2.0))), schema)
      val sut = getDetailDistribution(containerDf, detailId)
      val dataFrequence: Array[Row] = sut.collect()

      assert(sut.isInstanceOf[DataFrame])
      assert(dataFrequence(0).getString(0).equals("3.0"))
      assert(dataFrequence(0).getLong(1).equals(1: Long))
      assert(dataFrequence(1).getString(0).equals("2.0"))
      assert(dataFrequence(1).getLong(1).equals(2: Long))
      assert(dataFrequence(2).getString(0).equals("1.0"))
      assert(dataFrequence(2).getLong(1).equals(1: Long))
    }
  }

  describe("updateQueryWithResults") {
    it("should set query field data_summary and distribution to dataFrequenceDf and dataSummaryDf") {
      val dataSummarySchema = StructType(List(
        StructField("min", DoubleType),
        StructField("avg", DoubleType),
        StructField("max", DoubleType),
        StructField("count", DoubleType)))

      val dataFrequenceSchema = StructType(List(
        StructField("detail", StringType),
        StructField("count", LongType)))

      val dataSummaryDf = sqlContext.createDataFrame(sc.parallelize(List[Row](Row(1.0, 2.0, 3.0, 2.0))), dataSummarySchema)
      val dataFrequenceDf = sqlContext.createDataFrame(sc.parallelize(List[Row](Row("val1", 1: Long), Row("val2", 4: Long), Row("val3", 0: Long))), dataFrequenceSchema)
      val query = new Query(UUID.fromString("ad200280-0608-11e6-9678-19f6b16af7f0"), UUID.fromString("b0ee4890-0608-11e6-9678-19f6b16af7f0"), null, null, null, null)
      val sut = updateQueryWithResults(sc, query, dataFrequenceDf, dataSummaryDf)
      val expected = new Query(UUID.fromString("ad200280-0608-11e6-9678-19f6b16af7f0"), UUID.fromString("b0ee4890-0608-11e6-9678-19f6b16af7f0"), null, null, Map("min" -> 1.0, "avg" -> 2.0, "max" -> 3.0, "count" -> 2.0), Map("val1" -> 1, "val2" -> 4, "val3" -> 0))
      assert(sut.equals(expected))
    }

    it("should set query field data_summary and distribution to null if dataFrequenceDf does not contain any elements") {
      val dataFrequenceDf = sc.emptyRDD[String].toDF()
      val query = new Query(UUID.fromString("ad200280-0608-11e6-9678-19f6b16af7f0"), UUID.fromString("b0ee4890-0608-11e6-9678-19f6b16af7f0"), null, null, null, null)
      val sut = updateQueryWithResults(sc, query, dataFrequenceDf, null)
      assert(sut.equals(query))
    }
  }

  describe("updateQueryTable") {
    it("should update row in Query table with query results") {
      val query = new Query(existingSiloId, existingQueryId, null, null, Map("min" -> 1.0, "avg" -> 2.0, "max" -> 3.0, "count" -> 2.0), Map("val1" -> 1, "val2" -> 4, "val3" -> 0))
      updateQueryTable(sc, query)
      val updatedQuery = findQuery(sc, existingSiloId, existingQueryId)
      assert(query.dataSummary.equals(updatedQuery.dataSummary))
      assert(query.distribution.equals(updatedQuery.distribution))
    }
  }

  describe("getDetailSparkDataType") {
    it("should return a string spark data type for the detail") {
      val sut = getDetailSparkDataType(detailsRdd, "base_data", "weight")
      assert(sut.isInstanceOf[String])
      assert(sut.equals("double"))
    }
  }

  describe("parseDataSummary") {
    it("should return a map of data summary") {
      val detailId = "height"
      val schema = StructType(List(StructField(detailId, DoubleType)))
      val dataSummaryDf = sqlContext.createDataFrame(sc.parallelize(List[Row](Row(1.0), Row(2.0), Row(3.0), Row(2.0))), schema)
      val dataSummaryRow = getDataSummary(dataSummaryDf, detailId).first()

      val sut = parseDataSummary(dataSummaryRow)
      assert(sut.size == 4)
      assert(sut.get("min").equals(Some(1.0)))
      assert(sut.get("avg").equals(Some(2.0)))
      assert(sut.get("max") equals (Some(3.0)))
      assert(sut.get("count").equals(Some(4.0)))
    }

    it("should return a map of not null data summary entries") {
      val detailId = "education"
      val schema = StructType(List(StructField(detailId, StringType)))
      val dataFrequenceDf = sqlContext.createDataFrame(sc.parallelize(List[Row](Row("School"), Row("School"), Row("School"), Row("University"))), schema)
      val dataSummaryRow = getDataSummary(dataFrequenceDf, detailId).first()

      val sut = parseDataSummary(dataSummaryRow)
      assert(sut.size == 1)
      assert(!sut.get("min").nonEmpty)
      assert(!sut.get("avg").nonEmpty)
      assert(!sut.get("max").nonEmpty)
      assert(sut.get("count").equals(Some(4.0)))
    }
  }

  describe("toSparkDataType") {
    it("should return a Spark data type string from a Notos DB data type") {
      val integer = toSparkDataType("integer")
      assert(integer.equals("int"))
      val double = toSparkDataType("double")
      assert(double.equals("double"))
      val string = toSparkDataType("string")
      assert(string.equals("string"))
    }
  }
}
