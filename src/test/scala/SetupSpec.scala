import org.apache.spark.SparkContext
import org.scalatest.FunSpec
import org.apache.log4j.Logger
import org.apache.log4j.Level
import Setup._

/**
  * Created by tokevonryberg on 27/03/16.
  */

class SetupSpec extends FunSpec {
  describe("connect") {
    it("should return a SparkContext") {
      val testSc = connect()
      assert(testSc.isInstanceOf[SparkContext])
      assert(!testSc.isStopped)
      testSc.stop()
    }
  }

  describe("setLogLevel") {
    it("should set log level to INFO") {
      setLogLevel(Level.INFO)
      assert(Logger.getLogger("org").getLevel.equals(Level.INFO))
      assert(Logger.getLogger("com").getLevel.equals(Level.INFO))
      assert(Logger.getLogger("akka").getLevel.equals(Level.INFO))
    }
    it("should set log level to WARN per default") {
      setLogLevel()
      assert(Logger.getLogger("org").getLevel.equals(Level.WARN))
      assert(Logger.getLogger("com").getLevel.equals(Level.WARN))
      assert(Logger.getLogger("akka").getLevel.equals(Level.WARN))
    }
  }
}
