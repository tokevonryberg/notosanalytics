import java.util.UUID

import Model.{Detail, Query}
import com.datastax.spark.connector._
import com.datastax.spark.connector.rdd.{CassandraTableScanRDD, CqlWhereClause}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Row, SQLContext}
import org.apache.spark.sql.functions.{avg, col, count, max, min}

import scala.util.Try
import Setup.connect
import com.datastax.driver.core.utils.UUIDs

object Main {

  val helpText =
    """
      |NotosAnalytics v1.0.0
      |
      |NotosAnalytics is a data processing engine built on NotosDB.
      |
      |Usage: spark-submit --class Main --master local[x] notosanalytics-1.0
      |.0.jar siloId queryId
      |       x is the number of cores to use
    """.stripMargin

  def main(args: Array[String]): Unit = {
    if (args.length == 0) {
      println(helpText)
      return
    } else if (args.length != 2)
      throw new IllegalArgumentException("Expected two arguments. Got: " +
        args.length)

    val siloId = Try(UUID.fromString(args(0)))
      .getOrElse(
        throw new IllegalArgumentException(
          "Argument siloId must be of type UUID. Got: " + args(0)
        )
      )

    val queryId = Try(UUID.fromString(args(1)))
      .getOrElse(throw new IllegalArgumentException("Argument queryId must be" +
        " of type UUID. Got: " + args(1)))

    val sc = connect()
    val sqlContext = new SQLContext(sc)

    val query = findQuery(sc, siloId, queryId)
    val entityId = query.selectClause._1
    val detailId = query.selectClause._2
    val entityDetailTuples = getEntityDetailTuplesFromQuery(entityId,
      detailId, query.whereClause)
    val details = findDetailsRdd(sc, siloId, entityDetailTuples)
    val whereClause = createWhereClauseString(details, query.whereClause)
    val containers = findContainersDf(sqlContext, siloId.toString)
    val filteredContainerIds = filterContainers(containers, whereClause)
    val containerAssociations = findContainerAssociations(sqlContext, siloId
      .toString, entityId, filteredContainerIds)

    val detailDf = getDetailFromContainers(containers, entityId, detailId,
      containerAssociations)
    val dataSummaryDf = getDataSummary(detailDf, detailId)
    val distributionDf = getDetailDistribution(detailDf, detailId)

    val updatedQuery = updateQueryWithResults(sc, query, distributionDf,
      dataSummaryDf)
    updateQueryTable(sc, updatedQuery)

    sc.stop()
  }

  def findQuery(sc: SparkContext, siloId: UUID, queryId: UUID): Query = {
    Try(sc.cassandraTable[Query]("notos_analytics", "query")
      .where("silo_id = ? ", siloId)
      .where("query_id = ?", queryId)
      .first())
      .getOrElse(
        throw new NoSuchElementException(
          "Query with silo_id=" + siloId +
            " and query_id=" + queryId + " does not exist."
        )
      )
  }

  def getEntityDetailTuplesFromQuery(entityId: String, detailId: String,
                                     whereClause: Map[(String, String),
                                       String]): Set[(String, String)] =
    whereClause.map(w => w._1).toSet[(String, String)].+((entityId, detailId))

  def findDetailsRdd(sc: SparkContext, siloId: UUID,
                     entityDetailTuples: Set[(String, String)]): RDD[Detail] = {
    val entityIds = entityDetailTuples.map(t => t._1)
    val detailIds = entityDetailTuples.map(t => t._2)
    sc.cassandraTable[Detail]("notos_metadata", "detail")
      .select("entity_id", "detail_id", "data_type")
      .where("silo_id = ?", siloId)
      .where("entity_id in ?", entityIds)
      .where("detail_id in ?", detailIds)
  }

  def createWhereClauseString(details: RDD[Detail], whereClause: Map[(String,
    String), String]): String = {
    (for (((entityId, detailId), clause) <- whereClause) yield {
      val dataType = getDetailSparkDataType(details, entityId, detailId)
      "(entity_id = '" + entityId + "' and " + col("x1." + detailId).cast
      (dataType) + " " + clause + ")"
    })
      .mkString(" and ")
  }

  def findContainersDf(sqlContext: SQLContext, siloId: String): DataFrame = {
    sqlContext.read
      .format("org.apache.spark.sql.cassandra")
      .options(Map("table" -> "container",
        "keyspace" -> "notos_data"))
      .load()
      .where(col("silo_id").equalTo(siloId.toString))
  }

  def filterContainers(containers: DataFrame, whereClause: String):
  List[String] = {
    if (!whereClause.equals(""))
      containers.where(whereClause).select("container_id").map(c => c
        .getString(0)).collect().toList
    else
      List[String]()
  }

  def findContainerAssociations(sqlContext: SQLContext, siloId: String,
                                entityId: String, containers: List[String]):
  DataFrame = {
    sqlContext.read
      .format("org.apache.spark.sql.cassandra")
      .options(Map("table" -> "container_association",
        "keyspace" -> "notos_data"))
      .load()
      .where(col("silo_id").equalTo(siloId))
      .where(col("to_entity_id").equalTo(entityId))
      .where(col("from_container_id").isin(containers: _*))
      .select("to_container_id")
  }

  def getDetailFromContainers(containers: DataFrame, entityId: String,
                              detailId: String, containersAssociations:
                              DataFrame): DataFrame = {
    val filterByContainers = containersAssociations.map(_.getString(0))
      .collect().toList
    if (filterByContainers.size > 0) {
      containers.where(col("container_id").isin(filterByContainers: _*))
        .where(col("entity_id").equalTo(entityId))
        .select(col("x1." + detailId))
    } else
      containers.where(col("entity_id").equalTo(entityId))
        .select(col("x1." + detailId))
  }

  def getDataSummary(detailDf: DataFrame,
                     detailId: String): DataFrame = {
    detailDf.agg(
      min(detailId).cast("double").as("min"),
      avg(detailId).cast("double").as("avg"),
      max(detailId).cast("double").as("max"),
      count(detailId).cast("double").as("count")
    )
  }

  def getDetailDistribution(detailDf: DataFrame,
                            detailId: String): DataFrame = {
    detailDf.groupBy(col(detailId).cast("string")).count()
  }

  def updateQueryWithResults(sc: SparkContext, query: Query, distributionDf:
  DataFrame, dataSummaryDf: DataFrame): Query = {
    var q = Query(query.siloId, query.queryId, query.selectClause, query
      .whereClause, null, null)
    val distributionRows = distributionDf.collect()
    if (distributionRows.size > 0) {
      val result = parseDataSummary(dataSummaryDf.first())
      val distribution = (for (row <- distributionRows) yield (row.getString
      (0) -> row.getLong(1).toInt)).toMap
      q = q.copy(dataSummary = result, distribution = distribution) //
      // properties of case class are immutable
    }

    q
  }

  def updateQueryTable(sc: SparkContext, query: Query): Unit = {
    sc.parallelize(Seq(query))
      .saveToCassandra("notos_analytics", "query", SomeColumns("silo_id",
        "query_id", "data_summary", "distribution"))
  }

  def getDetailSparkDataType(details: RDD[Detail], entityId: String,
                             detailId: String): String = {
    val detail = details.filter(d => d.entityId.equals(entityId) && d
      .detailId.equals(detailId)).first()
    toSparkDataType(detail.dataType)
  }

  def parseDataSummary(dataSummary: Row): Map[String, Double] = {
    (for (i <- 0 until dataSummary.size) yield {
      if (!dataSummary.isNullAt(i)) {
        Tuple2(dataSummary.schema(i).name, dataSummary.getDouble(i))
      }
    }).filter(_ != (())) // remove Boxedunit
      .map(x => x.asInstanceOf[Tuple2[String, Double]]).toMap
  }

  def toSparkDataType(dataType: String): String = dataType match {
    case "integer" => "int"
    case "date" | "double" | "string" => dataType
  }

}
