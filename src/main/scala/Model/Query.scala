package Model

import java.util.UUID

case class Query(
                  siloId: UUID,
                  queryId: UUID,
                  selectClause: Tuple2[String, String],
                  whereClause: Map[Tuple2[String, String], String],
                  dataSummary: Map[String, Double],
                  distribution: Map[String, Int]
                )
