package Model

case class Detail(
                   entityId: String,
                   detailId: String,
                   dataType: String
                 )
