import org.apache.spark.{SparkConf, SparkContext}
import org.apache.log4j.Logger
import org.apache.log4j.Level

object Setup {

  setLogLevel(Level.WARN)

  val conf = new SparkConf(true)
    .set("spark.cassandra.connection.host", "127.0.0.1")
    .set("spark.cassandra.auth.username", "cassandra")
    .set("spark.cassandra.auth.password", "cassandra")

  def connect(): SparkContext = {
    new SparkContext(conf)
  }

  def setLogLevel(lvl: Level = Level.WARN) {
    Logger.getLogger("org").setLevel(lvl)
    Logger.getLogger("com").setLevel(lvl)
    Logger.getLogger("akka").setLevel(lvl)
  }


}
