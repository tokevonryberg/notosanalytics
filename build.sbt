
name := "NotosAnalytics"
version := "1.0.0"
scalaVersion := "2.10.6"
libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.6.1",
  "org.apache.spark" %% "spark-sql" % "1.6.1",
  "org.apache.spark" %% "spark-mllib" % "1.6.1",
  "com.datastax.spark" %% "spark-cassandra-connector" % "1.6.0-M1",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test",
  "com.holdenkarau" %% "spark-testing-base" % "1.6.1_0.3.3"
)
parallelExecution in Test := false
parallelExecution in ThisBuild := false
parallelExecution in Global := false
assemblyJarName in assembly := s"${name.value.toLowerCase()}-${version.value}.jar"
test in assembly := {}

assemblyMergeStrategy in assembly := {
  case x if x.endsWith(".class") => MergeStrategy.last
  case x if x.endsWith(".properties") => MergeStrategy.last
  case x if x.contains("/resources/") => MergeStrategy.last
  case x if x.startsWith("META-INF/mailcap") => MergeStrategy.last
  case x if x.startsWith("META-INF/mimetypes.default") => MergeStrategy.first
  case x if x.startsWith("META-INF/maven/org.slf4j/slf4j-api/pom.") => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    if (oldStrategy == MergeStrategy.deduplicate)
      MergeStrategy.first
    else
      oldStrategy(x)
}
